import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Progress } from "antd";
export default function DetailPage() {
  let { id } = useParams();
  const [moive, setMoive] = useState([]);

  useEffect(() => {
    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjAzLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NTM4MjQwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc1NTMwMDAwfQ.if3ZZ_VKK8nppxZJ2DF4FGoRPCmaYx6ncNAQytkjIT0",
      },
    })
      .then((res) => {
        console.log("res: ", res);
        setMoive(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  // Progress
  return (
    <div className="row">
      <img className="col-4" src={moive.hinhAnh} alt="" />

      <Progress
        format={(number) => {
          return <span className="text-danger">{number / 10 + " Điểm"} </span>;
        }}
        strokeColor={"red"}
        type="circle"
        percent={moive.danhGia * 10}
      />
    </div>
  );
}
