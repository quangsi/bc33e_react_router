import React, { memo } from "react";

function HeaderHook({ like, handlePlusLike }) {
  console.log("Header render");
  return (
    <div className="p-5 bg-primary text-white">
      HeaderHook
      <p className="display-4">Like: {like}</p>
      <button onClick={handlePlusLike} className="btn btn-success">
        Plus like
      </button>
    </div>
  );
}
export default memo(HeaderHook);

// redux thunk
