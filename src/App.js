import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import LoginPage from "./pages/LoginPage/LoginPage";
import DetailPage from "./pages/DetailPage/DetailPage";
import DemoHookPage from "./pages/DemoHookPage/DemoHookPage";
import Navbar from "./components/Navbar";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
function App() {
  // state
  return (
    <>
      <BrowserRouter>
        <Navbar />
        {/* <Switch> */}
        <Route exact path="/" component={HomePage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/detail/:id" component={DetailPage} />
        {/* <Route path="/demo-hook" component={DemoHookPage} /> */}
        <Route
          path="/demo-hook"
          render={() => {
            return <DemoHookPage />;
          }}
        />
        {/* </Switch> */}
      </BrowserRouter>
    </>
  );
}

export default App;
