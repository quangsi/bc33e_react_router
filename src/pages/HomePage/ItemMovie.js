import { Card } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ItemMovie({ dataMovie }) {
  console.log("dataMovie: ", dataMovie);
  return (
    <Card
      hoverable
      style={{
        width: "100%",
      }}
      cover={<img alt="example" src={dataMovie.hinhAnh} />}
    >
      <Meta
        title={<p className="text-success">{dataMovie.tenPhim}</p>}
        description="www.instagram.com"
      />
      <NavLink to={`/detail/${dataMovie.maPhim}`}>
        <button className="btn btn-danger">Xem chi tiết</button>
      </NavLink>
    </Card>
  );
}
