import axios from "axios";
import React, { useEffect, useState } from "react";
import ItemMovie from "./ItemMovie";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCDEkMOgIE7hurVuZyAwMyIsIkhldEhhblN0cmluZyI6IjAzLzAyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3NTM4MjQwMDAwMCIsIm5iZiI6MTY0NTgwODQwMCwiZXhwIjoxNjc1NTMwMDAwfQ.if3ZZ_VKK8nppxZJ2DF4FGoRPCmaYx6ncNAQytkjIT0",
      },
    })
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderMovieList = () => {
    return movies.map((item, index) => {
      return (
        <div key={item.maPhim} className="col-2">
          <ItemMovie dataMovie={item} />
        </div>
      );
    });
  };

  return <div className="row">{renderMovieList()}</div>;
}

// render chạy trước =>1
