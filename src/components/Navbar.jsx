import React from "react";
import { Link, NavLink } from "react-router-dom";

export default function Navbar() {
  return (
    <div className=" text-center p-5">
      <NavLink
        activeClassName="btn-success"
        className="btn  btn-primary"
        to="/"
        exact
      >
        Home page
      </NavLink>
      <NavLink
        activeClassName="btn-success"
        className="btn  btn-primary mx-5"
        to="/demo-hook"
      >
        Demo Hook page
      </NavLink>
    </div>
  );
}
