import React, { useCallback, useEffect, useMemo, useState } from "react";
import HeaderHook from "./HeaderHook";

export default function DemoHookPage() {
  // state={
  //  like:1
  // }

  let [like, setLike] = useState(1);
  let [share, setShare] = useState(100);
  const [scores, setScores] = useState([1, 5, 7, 8, 4]);

  useEffect(() => {
    // ~ tương tự componentDidMount
    // Gọi api tại đây
    console.log("yes - useEffect");
  }, [share]);
  //  [] trong useEffect : Dependency ~ Quan sát giá trị

  // let handlePlusLike = () => {
  //   setLike(like + 1);

  // };

  let handlePlusLike = useCallback(() => {
    setLike(like + 1);
  }, [like]);

  let handlePlusShare = () => {
    setShare(share + 1);
  };

  // useMemo
  // let totalScore = scores.reduce((pre, current) => {
  //   console.log("Chạy lại r nè");
  //   return pre + current;
  // }, 0);
  let totalScore = useMemo(() => {
    return scores.reduce((pre, current) => {
      console.log("Chạy lại r nè");
      return pre + current;
    }, 0);
  }, [scores]);

  console.log("totalScore: ", totalScore);
  console.log("component render");
  return (
    <div className="text-center">
      <HeaderHook handlePlusLike={handlePlusLike} like={like} />
      {/* 
      like
      */}
      <span className="text-warning display-4">{like}</span>
      <button onClick={handlePlusLike} className="btn btn-warning">
        Plus like
      </button>
      {/* share */}
      <span className="text-danger display-4">{share}</span>
      <button onClick={handlePlusShare} className="btn btn-danger">
        Plus share
      </button>
      <br />

      <h2>Total score:{totalScore}</h2>
    </div>
  );
}

// let colors = ["red", "green"];

// let [c1, c2] = colors;
